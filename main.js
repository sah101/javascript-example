var header = document.getElementById("main-header");
var colorButton = document.getElementById("color-change");
var paragraphButton = document.getElementById("add-p");
var body = document.getElementById("body");
var headerSection = document.getElementById("header");
var footerSection = document.getElementById("footer");
var showButtons = document.getElementById("show-buttons");
var author = document.getElementById("author");
var menu = document.getElementsByClassName("menu-item");

header.addEventListener("click", function(e){
	var headerText = prompt("What should this header say?");
	header.innerHTML = headerText;
});

author.addEventListener("click", function(e){
	var authorText = prompt("What is your name?");
	author.innerHTML = authorText;
});

colorButton.addEventListener("mouseenter", function(){
	headerSection.style.backgroundColor = "pink";
	footerSection.style.backgroundColor = "pink";
	body.style.backgroundColor = "purple";
});

colorButton.addEventListener("mouseleave", function(){
	headerSection.style.backgroundColor = "gray";
	footerSection.style.backgroundColor = "gray";
	body.style.backgroundColor = "lightgray";
});

showButtons.addEventListener("click", function(){
	colorButton.classList.toggle('hidden');
	paragraphButton.classList.toggle('hidden');
});

paragraphButton.addEventListener("click", function(){
	var text = prompt("What do you want to add?");
	var p = document.createElement("p");
	p.innerHTML = text;
	body.appendChild(p);
	
});

for(var i = 0; i<menu.length; i++){
    menu[i].addEventListener("click", function(e){
    	var subItems = e.target.getElementsByClassName("sub-menu-item");
    	for (var i = 0; i<subItems.length; i++){
    		subItems[i].classList.toggle('hidden');
    	}
	});
}
